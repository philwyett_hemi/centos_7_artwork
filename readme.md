# Read Me

This repo contains a collection of centos 7.x artwork.

# License

The artwork created by the author is released under the GPL 3 license.

[GPL 3 License](https://www.gnu.org/licenses/gpl-3.0.en.html)

CentOS and it's brand are covered by their own licensing and guidelines.

[CentOS trademarks guide](https://www.centos.org/legal/trademarks/)

# Information

#### File: grey_centos_symbol.xcf

Gimp multi-layer master image.

#### Ready mades:

grey_centos_symbol_1366_x_768.jpg

grey_centos_symbol_1920_x_1080.jpg

grey_centos_symbol_3840_x_2160.jpg (recommended size)

#### File: plasma_centos.xcf

Gimp multi-layer master image.

#### Ready mades:

plasma_centos_1366_x_768.jpg

plasma_centos_1920_x_1080.jpg

plasma_centos_3840_x_2160.jpg (recommended size)
